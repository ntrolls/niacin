package com.niacin.persistence;


public abstract class Bag 
{
	protected String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
