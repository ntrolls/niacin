package com.niacin.persistence;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import com.google.gson.InstanceCreator;
import com.niacin.input.IntegerVariable;
import com.niacin.input.Variable;

public class VariableInstanceCreator implements InstanceCreator<Variable<?>>
{
	@Override
	public Variable<?> createInstance(Type type) 
	{
		Type[] typeParameters = ((ParameterizedType)type).getActualTypeArguments();
	    Type idType = typeParameters[0];
	    if(idType.equals(Integer.class))
	    	return new IntegerVariable();
		return null ;
	}

}
