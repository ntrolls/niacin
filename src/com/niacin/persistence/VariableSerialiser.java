package com.niacin.persistence;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.niacin.input.IntegerVariable;
import com.niacin.input.Variable;

public class VariableSerialiser implements JsonSerializer<Variable<?>>
{
	private Gson gson = new Gson();
	
	@Override
	public JsonElement serialize(Variable<?> src, Type typeOfSrc, JsonSerializationContext context) 
	{
		Type[] typeParameters = ((ParameterizedType)typeOfSrc).getActualTypeArguments();
	    Type idType = typeParameters[0];
	    System.out.println(idType);
	    System.out.println(typeOfSrc);
	    if(idType.equals(Integer.class))
	    {
	    	System.out.println("ha!");
	    	return new JsonPrimitive(gson.toJson((IntegerVariable)src));
	    }
		return null; 
	}

}
