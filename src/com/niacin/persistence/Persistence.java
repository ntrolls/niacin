package com.niacin.persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;

import com.thoughtworks.xstream.XStream;

public class Persistence
{

	//	private static Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(Variable.class, new VariableInstanceCreator()).registerTypeAdapter(Variable.class, new VariableSerialiser()).create();
	private static XStream xstr = new XStream();

	public static void store(Bag bag)
	{
		xstr.setMode(XStream.NO_REFERENCES);
		try
		{
			FileWriter writer = new FileWriter(new File(bag.getName() + ".txt"));
			writer.write(xstr.toXML(bag));
			writer.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static Bag load(String name, Class<? extends Bag> klass)
	{
		try
		{
			LineNumberReader reader = new LineNumberReader(new FileReader(new File(name + ".txt")));
			StringBuffer buf = new StringBuffer();
			String read;
			while ((read = reader.readLine()) != null)
				buf.append(read);
			String xml = buf.toString();
			return (Bag) xstr.fromXML(xml);
		}
		catch (FileNotFoundException e)
		{
			return null;
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static String toXML(Object obj)
	{
		return xstr.toXML(obj);
	}
}
