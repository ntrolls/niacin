package com.niacin.persistence;

import java.util.ArrayList;

import com.niacin.input.Solution;

public class HCBag extends Bag
{

	int state;
	int remainingEvaluations;
	Solution currentSolution;
	Solution bestSoFarSolution;
	ArrayList<Solution> neighbours;
	int neighbourPos;

	public HCBag()
	{
		super();
	}

	public int getState()
	{
		return state;
	}
	public void setState(int state)
	{
		this.state = state;
	}
	public int getRemainingEvaluations()
	{
		return remainingEvaluations;
	}
	public void setRemainingEvaluations(int remainingEvaluations)
	{
		this.remainingEvaluations = remainingEvaluations;
	}
	public Solution getBestSoFarSolution()
	{
		return bestSoFarSolution;
	}

	public void setBestSoFarSolution(Solution bestSoFarSolution)
	{
		this.bestSoFarSolution = bestSoFarSolution;
	}
	public ArrayList<Solution> getNeighbours()
	{
		return neighbours;
	}
	public void setNeighbours(ArrayList<Solution> newNeighbours)
	{
		this.neighbours = newNeighbours;
	}
	public Solution getCurrentSolution()
	{
		return currentSolution;
	}

	public void setCurrentSolution(Solution currentSolution)
	{
		this.currentSolution = currentSolution;
	}

	public int getNeighbourPos()
	{
		return neighbourPos;
	}

	public void setNeighbourPos(int neighbourPos)
	{
		this.neighbourPos = neighbourPos;
	}
}
