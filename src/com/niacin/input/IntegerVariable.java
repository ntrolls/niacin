package com.niacin.input;

import java.lang.reflect.Method;
import java.util.Random;

import com.niacin.annotation.Input;

public class IntegerVariable extends Variable<Integer>
{
	public IntegerVariable()
	{

	}

	public static IntegerVariable parse(Method m)
	{
		IntegerVariable v = new IntegerVariable();
		Input ann = m.getAnnotation(Input.class);
		v.name = ann.name();
		v.current = Integer.parseInt(ann.initvalue());
		v.step = Integer.parseInt(ann.stepvalue());
		System.out.println(ann.stepvalue());
		if (ann.bound().length() > 0)
		{
			v.lbound = Integer.parseInt(ann.bound().split(",")[0].trim());
			v.ubound = Integer.parseInt(ann.bound().split(",")[1].trim());
		}
		else
		{
			v.lbound = -1;
			v.ubound = -1;
		}
		return v;
	}

	@Override
	public Variable<Integer> random()
	{
		Random random = new Random();
		Variable<Integer> r = this.clone();
		if (lbound == -1 && ubound == -1)
			r.current = random.nextInt();
		else
			r.current = random.nextInt(ubound - lbound + 1) + lbound;
		r.step = 1;
		return r;
	}

	@Override
	public Variable<Integer> increase()
	{
		if (current == ubound)
			return null;
		Variable<Integer> r = this.clone();
		r.current = this.current + this.step();
		return r;
	}

	@Override
	public Variable<Integer> decrease()
	{
		if (current == lbound)
			return null;
		Variable<Integer> r = this.clone();
		r.current = this.current - this.step();
		return r;
	}

	@Override
	public Variable<Integer> clone()
	{
		Variable<Integer> nv = new IntegerVariable();
		nv.name = this.name;
		nv.step = this.step;
		nv.lbound = this.lbound;
		nv.ubound = this.ubound;
		nv.current = this.current;
		return nv;
	}
}
