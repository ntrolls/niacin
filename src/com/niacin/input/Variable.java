package com.niacin.input;

public abstract class Variable<T>
{
	protected String name;
	protected T step;
	protected T lbound;
	protected T ubound;
	protected T current;

	public String name()
	{
		return name;
	}

	public T step()
	{
		return step;
	}

	public T lbound()
	{
		return lbound;
	}

	public T ubound()
	{
		return ubound;
	}

	public abstract Variable<T> random();

	public T current()
	{
		return current;
	}

	public abstract Variable<T> increase();
	public abstract Variable<T> decrease();

	public abstract Variable<T> clone();

	public String toString()
	{
		return name() + ":" + current() + " ";
	}
}
