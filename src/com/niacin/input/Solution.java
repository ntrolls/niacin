package com.niacin.input;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Solution implements Iterable<Variable<?>> 
{
	private boolean evaluated = false;
	private double fitness = 0;
	private ArrayList<Variable<?>> variables = new ArrayList<Variable<?>>();
	
	public double getFitness()
	{
		return fitness;
	}
	
	public void setFitness(int f)
	{
		fitness = (double)f;
	}
	
	public void setFitness(double f)
	{
		fitness = f;
	}

	public boolean isEvaluated() {
		return evaluated;
	}

	public void setEvaluated(boolean evaluated) {
		this.evaluated = evaluated;
	}
	
	public Variable<?> findVariableWithName(String name) {
		Iterator<Variable<?>> it = variables.iterator();
		while(it.hasNext())
		{
			Variable<?> v = it.next();
			if(v.name().equals(name)) return v;
		}
		return null;
	}
	
	public Object getCurrentValue(String name)
	{
		return findVariableWithName(name).current();
	}
	
	public String toString()
	{
		StringBuffer buf = new StringBuffer();
		Iterator<Variable<?>> it = variables.iterator();
		while(it.hasNext())
		{
			Variable<?> v = it.next();
			buf.append(v.toString());
		}
		return buf.toString();
	}

	@Override
	public Object clone() 
	{
		Solution cloned = new Solution();
		cloned.evaluated = this.evaluated;
		cloned.fitness = this.fitness;
		Iterator<Variable<?>> it = variables.iterator();
		while(it.hasNext())
		{
			Variable<?> var = it.next();
			cloned.add(var.clone());
		}
		return cloned;
	}

	public void addAll(Collection<Variable<?>> vars) 
	{
		variables.addAll(vars);
	}

	public void add(Variable<?> var) 
	{
		variables.add(var);
		
	}

	public Variable<?> get(int varNum) 
	{
		return variables.get(varNum);
	}

	public void set(int varNum, Variable<?> var) {
		variables.set(varNum, var);
	}

	public int size() {
		return variables.size();
	}

	@Override
	public Iterator<Variable<?>> iterator() 
	{
		return variables.iterator();
	}
	
	
}
