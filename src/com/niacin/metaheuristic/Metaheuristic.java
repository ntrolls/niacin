package com.niacin.metaheuristic;

import com.niacin.input.Solution;
import com.niacin.persistence.Bag;
import com.niacin.problem.Problem;

public interface Metaheuristic 
{
	public enum Type {HC};
	
	public Solution getNextInput(Problem problem);
	public void recordFitness(Problem problem, double fitness);
	
	public Bag pack(Problem problem);
	public void unpack(Bag bag);
}
