package com.niacin.metaheuristic;

import com.niacin.metaheuristic.hillclimbing.HillClimbing;
import com.niacin.problem.Problem;

public class MetaheuristicFactory {

	public static Metaheuristic create(Metaheuristic.Type type, Problem problem) 
	{
		switch(type)
		{
		case HC:
			HillClimbing hc = new HillClimbing();
			return hc; 
		}
		return null;
	}

}
