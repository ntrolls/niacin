package com.niacin.metaheuristic;

import com.niacin.input.Solution;
import com.niacin.problem.Problem;

public interface FitnessComparator
{
	public double compare(Problem problem, Solution s1, Solution s2);
}
