package com.niacin.metaheuristic;

public interface Fitness <T> 
{
	public T fitness();
	public void recordFitness(T f);
}
