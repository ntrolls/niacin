package com.niacin.metaheuristic;

import com.niacin.annotation.Optimize;
import com.niacin.input.Solution;
import com.niacin.problem.Problem;

public class SimpleFitnessComparator implements FitnessComparator
{

	@Override
	public double compare(Problem problem, Solution s1, Solution s2)
	{
		if (problem.getDirection() == Optimize.Direction.MIN)
			return s2.getFitness() - s1.getFitness();
		else
			return s1.getFitness() - s2.getFitness();
	}

}
