package com.niacin.metaheuristic.hillclimbing;

import java.util.ArrayList;

import com.niacin.input.Solution;
import com.niacin.input.Variable;

public class NeighbourhoodGenerator
{
	private enum Direction
	{
		increase, decrease
	};

	public static ArrayList<Solution> generateNeighbours(Solution s)
	{
		ArrayList<Solution> neighbours = new ArrayList<Solution>();
		for (int i = 0; i < s.size(); i++)
		{
			Solution si = makeMove(s, Direction.increase, i);
			if (si != null)
				neighbours.add(si);
			Solution sd = makeMove(s, Direction.decrease, i);
			if (si != null)
				neighbours.add(sd);
		}
		return neighbours;
	}

	private static Solution makeMove(Solution original, Direction dir, int varNum)
	{
		Solution sol = (Solution) original.clone();
		sol.setEvaluated(false);
		Variable<?> v = sol.get(varNum);
		Variable<?> changed = null;
		switch (dir)
		{
			case increase :
				changed = v.increase();
				break;
			case decrease :
				changed = v.decrease();
				break;
		}
		if (changed != null)
		{
			sol.set(varNum, v.increase());
			return sol;
		}
		else
			return null;
	}
}