package com.niacin.test;

import com.niacin.annotation.Input;
import com.niacin.annotation.Optimize;

public class HCTestTarget
{
	private int input = 0;

	@Input(name = "input", initvalue = "0", bound = "0, 30")
	public void setInput(Integer input)
	{
		this.input = input;
	}

	@Optimize(type = Double.class, direction = Optimize.Direction.MIN, trials = 30)
	public Double getDistance()
	{
		System.out.println("measured fitness (target):" + (double) Math.abs(10 - this.input));
		return (double) Math.abs(10 - this.input);
	}
}
