package com.niacin.test;

import org.junit.Test;

import com.niacin.main.Niacin;

public class HCTest
{

	@Test
	public void test()
	{
		HCTestTarget target = new HCTestTarget();

		Niacin.initialise(HCTestTarget.class);
		Niacin.start(target);
		Niacin.end(target);
	}

}
