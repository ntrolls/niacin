package com.niacin.test;

import java.lang.reflect.Method;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.niacin.input.IntegerVariable;
import com.niacin.input.Solution;
import com.niacin.input.Variable;
import com.niacin.persistence.VariableInstanceCreator;
import com.niacin.persistence.VariableSerialiser;

public class InstanceCreatorTest {

	@Test
	public void test() throws SecurityException, NoSuchMethodException 
	{
		Gson gson = new GsonBuilder().registerTypeAdapter(Variable.class, new VariableInstanceCreator()).registerTypeAdapter(Variable.class, new VariableSerialiser()).create();
		Class<?> klass = HCTestTarget.class;
		Method m = klass.getMethod("setInput", new Class<?>[]{Integer.class});
		IntegerVariable v = IntegerVariable.parse(m);
		
		Variable<Integer> p = v;
		System.out.println(gson.toJson(p));
		
		Solution s = new Solution();
		s.add(p);
		System.out.println(gson.toJson(s));
	}

}
