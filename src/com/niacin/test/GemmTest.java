package com.niacin.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class GemmTest
{
	@Test
	public void test()
	{
		int size = 1000;
		double[] A = new double[size * size];
		double[] B = new double[size * size];

		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
			{
				A[i * size + j] = 0;
				B[i * size + j] = 0;
				if (i == 0)
					A[i * size + j] = 1;
				if (j == 0)
					B[i * size + j] = 1;
			}

		double[] C2 = new double[size * size];

		BlockedMatrixMultiplication bmm = new BlockedMatrixMultiplication();
		bmm.square_dgemm(size, size, size, A, B, C2);

		assertTrue((int) C2[0] == size);
	}
}
