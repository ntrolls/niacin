package com.niacin.test;

import java.lang.reflect.Method;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.niacin.input.IntegerVariable;
import com.niacin.input.Solution;
import com.niacin.input.Variable;
import com.niacin.persistence.VariableInstanceCreator;
import com.niacin.persistence.VariableSerialiser;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class PersistenceTest
{

	@Test
	public void test() throws SecurityException, NoSuchMethodException
	{
		Gson gson = new GsonBuilder().registerTypeAdapter(Variable.class, new VariableInstanceCreator()).registerTypeAdapter(Variable.class, new VariableSerialiser()).create();
		Class<?> klass = HCTestTarget.class;
		Method m = klass.getMethod("setInput", new Class<?>[]{Integer.class});
		IntegerVariable v = IntegerVariable.parse(m);

		Variable<?> p = v;
		System.out.println(gson.toJson(p));

		Solution s = new Solution();
		s.add(p);
		System.out.println(gson.toJson(s));

		XStream xstream = new XStream(new DomDriver());
		String xml = xstream.toXML(s);
		System.out.println(xml);

		Solution s2 = (Solution) xstream.fromXML(xml);
		System.out.println(xstream.toXML(s2));
	}
}
