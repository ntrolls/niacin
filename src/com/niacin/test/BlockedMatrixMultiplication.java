package com.niacin.test;

import com.niacin.annotation.Input;
import com.niacin.annotation.Optimize;
import com.niacin.main.Niacin;

public class BlockedMatrixMultiplication
{
	private int BLOCK_SIZE = 8;
	private double rate = 0;

	@Input(name = "block_size", initvalue = "8", bound = "0, 128")
	public void setBlockSize(int size)
	{
		this.BLOCK_SIZE = size;
	}

	@Optimize(name = "rate", type = Double.class, direction = Optimize.Direction.MAX)
	public Double getRate()
	{
		return new Double(rate);
	}

	public BlockedMatrixMultiplication()
	{
		Niacin.initialise(BlockedMatrixMultiplication.class);
	}

	public void basic_dgemm(int lda, int M, int N, int K, double[] A, double[] B, double[] C)
	{
		int i, j, k;

		/*
		  To optimize this, think about loop unrolling and software
		  pipelining.  Hint:  For the majority of the matmuls, you
		  know exactly how many iterations there are (the block size)...
		*/

		for (i = 0; i < M; ++i)
		{
			for (j = 0; j < N; ++j)
			{
				double cij = C[i * lda + j];
				for (k = 0; k < K; ++k)
				{
					cij += A[i * lda + k] * B[k * lda + j];
				}

				C[i * lda + j] = cij;
			}
		}
	}

	public void do_block(int lda, double[] A, double[] B, double[] C, int i, int j, int k)
	{
		/*
		  Remember that you need to deal with the fringes in each
		  dimension.

		  If the matrix is 7x7 and the blocks are 3x3, you'll have 1x3,
		  3x1, and 1x1 fringe blocks.

		        xxxoooX
		        xxxoooX
		        xxxoooX
		        oooxxxO
		        oooxxxO
		        oooxxxO
		        XXXOOOX

		  You won't get this to go fast until you figure out a `better'
		  way to handle the fringe blocks.  The better way will be more
		  machine-efficient, but very programmer-inefficient.
		*/
		int M = (i + BLOCK_SIZE > lda ? lda - i : BLOCK_SIZE);
		int N = (j + BLOCK_SIZE > lda ? lda - j : BLOCK_SIZE);
		int K = (k + BLOCK_SIZE > lda ? lda - k : BLOCK_SIZE);

		//System.out.println(M + ", " + N + ", " + K + ", " + i + ", " + j + ", " + k);
		//basic_dgemm (lda, M, N, K, A + i + k*lda, B + k + j*lda, C + i + j*lda);
		for (int _i = 0; _i < M; _i++)
		{
			for (int _j = 0; _j < N; _j++)
			{
				double _cij = C[j + i * lda + _j + _i * lda];
				for (int _k = 0; _k < K; _k++)
				{
					_cij += A[i * lda + k + _i * lda + _k] * B[j + k * lda + _j + _k * lda];
					//					System.out.print("C[" + (j + i * lda + _j + _i * lda) + "] += A[" + (i * lda + k + _i * lda + _k) + "] * B[" + (j + k * lda + _j + _k * lda) + "]");
					//					System.out.println((A[i * lda + k + _i * lda + _k] * B[j + k * lda + _j + _k * lda]) + " (" + A[i * lda + k + _i * lda + _k] + " * " + B[j + k * lda + _j + _k * lda] + ")");
					//					System.out.println(_cij + ":" + C[0]);
				}
				C[j + i * lda + _j + _i * lda] = _cij;
			}
		}
	}

	public void square_dgemm(int M, int N, int K, double[] A, double[] B, double[] C)
	{
		Niacin.start(this);

		long start = System.currentTimeMillis();

		int n_blocks = M / BLOCK_SIZE + (M % BLOCK_SIZE > 0 ? 1 : 0);
		int bi, bj, bk;

		for (bi = 0; bi < n_blocks; ++bi)
		{
			int i = bi * BLOCK_SIZE;
			for (bj = 0; bj < n_blocks; ++bj)
			{
				int j = bj * BLOCK_SIZE;
				for (bk = 0; bk < n_blocks; ++bk)
				{
					int k = bk * BLOCK_SIZE;
					do_block(M, A, B, C, i, j, k);
				}
			}
		}

		long elapsed = System.currentTimeMillis() - start;
		rate = ((double) M * N * K) / (double) elapsed;

		Niacin.end(this);
	}

	public int getBLOCK_SIZE()
	{
		return BLOCK_SIZE;
	}

	public void setBLOCK_SIZE(int bLOCK_SIZE)
	{
		BLOCK_SIZE = bLOCK_SIZE;
	}
}
