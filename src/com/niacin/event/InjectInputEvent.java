package com.niacin.event;

public class InjectInputEvent
{
	Object source = null;

	public InjectInputEvent(Object source)
	{
		this.source = source;
	}

	public Object getSource()
	{
		return this.source;
	}

}
