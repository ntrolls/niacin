package com.niacin.event;

public class MeasureFitnessEvent
{
	Object source = null;

	public MeasureFitnessEvent(Object source)
	{
		this.source = source;
	}

	public Object getSource()
	{
		return this.source;
	}

}
