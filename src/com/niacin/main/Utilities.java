package com.niacin.main;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Utilities {

	public static Method findMethodWithAnnotation(Class<?> klass, Class<? extends Annotation> annotation)
	{
		for(Method m: klass.getMethods())
			if(m.isAnnotationPresent(annotation))
				return m;
		return null;
	}
	
	public static Object parse(Class<?> type, String str) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException
	{
		Method valueOf = type.getMethod("valueOf", String.class);
		Object value = valueOf.invoke(null, str);
		return value;
	}
}
