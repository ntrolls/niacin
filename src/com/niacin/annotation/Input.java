package com.niacin.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Input
{
	String name() default "variable";
	String initvalue();
	String stepvalue() default "1";
	String bound() default "";
}
