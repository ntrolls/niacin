package com.niacin.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.niacin.metaheuristic.Metaheuristic;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Optimize
{
	public enum Direction
	{
		MAX, MIN
	};

	Class<?> type();
	String name() default "fitness";
	Direction direction() default Direction.MIN;
	
	int trials() default 100;
	Metaheuristic.Type method() default Metaheuristic.Type.HC;

}
