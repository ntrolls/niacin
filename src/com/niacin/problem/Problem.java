package com.niacin.problem;

import java.util.Vector;

import com.niacin.annotation.Optimize;
import com.niacin.input.Solution;
import com.niacin.input.Variable;

public class Problem
{
	private Vector<Variable<?>> variables = new Vector<Variable<?>>();
	private String name;
	private int remainingTrials;
	private Optimize.Direction direction;

	public Problem(String name, Vector<Variable<?>> variables, Optimize.Direction direction, int trials)
	{
		this.name = name;
		this.variables.addAll(variables);
		this.direction = direction;
		this.remainingTrials = trials;
	}

	public Solution initialSolution()
	{
		Solution initial = new Solution();
		initial.addAll(variables);
		return initial;
	}

	public Solution generateRandomRestart()
	{
		Solution random = new Solution();
		for (Variable<?> v : variables)
		{
			random.add(v.random());
		}
		return random;
	}

	public String name()
	{
		return name;
	}

	public Optimize.Direction getDirection()
	{
		return direction;
	}

	public int getRemainingTrials()
	{
		return remainingTrials;
	}
}
